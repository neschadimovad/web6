function poleKol() {
    let k = document.getElementById("kol").value;
    if (k.match(/^[0-9]+$/) === null) {
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;
    } else {
        return true;
    }
}

function polePrice() {
    return {
        iPhone: [45000, 65000, 40000],
        vibor: {
            opt1: 0,
            opt2: 3000,
            opt3: 7000,
        },
        doppole: {
            dop1: 500,
            dop2: 400,
            dop3: 18500,
        },
    };
}

function price() {
    let vid = document.getElementById("iPhone");
    let kol = document.getElementById("kol").value;
    let radk = document.getElementById("radio");
    let box = document.getElementById("checkbox");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    switch (vid.value) {
        case "1":
            radk.style.display = "none";
            box.style.display = "none";
            if (poleKol()) {
                let itog = parseInt(kol) * polePrice().iPhone[0];
                price.innerHTML = itog + " рублей";
            }
            break;
        case "2":
            radk.style.display = "block";
            box.style.display = "none";
            let per;
            let radbut = document.getElementsByName("vibor");
            for (var i = 0; i < radbut.length; i++) {
                if (radbut[i].checked) {
                    per = radbut[i].value;
                }
            }
            let stoit = polePrice().vibor[per];
            if (poleKol()) {
                let itog = parseInt(kol) * (polePrice().iPhone[1] + stoit);
                price.innerHTML = itog + " рублей";
            }
            break;
        case "3":
            radk.style.display = "none";
            box.style.display = "block";
            let sumop = 0;
            let d = document.getElementsByName("dop");
            for (let i = 0; i < d.length; i++) {
                if (d[i].checked) {
                    sumop += polePrice().doppole[d[i].value];
                }
            }
            if (poleKol()) {
                let itog = parseInt(kol) * (polePrice().iPhone[2] + sumop);
                price.innerHTML = itog + " рублей";
            }
            break;
    }
}

window.addEventListener("DOMContentLoaded", function(event) {
    let radk = document.getElementById("radio");
    radk.style.display = "none";
    let box = document.getElementById("checkbox");
    box.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function(event) {
        console.log("Kol was changed");
        price();
    });

    let select = document.getElementById("iPhone");
    select.addEventListener("change", function(event) {
        console.log("Model was changed");
        price();
    });

    let radio = document.getElementsByName("vibor");
    radio.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            console.log("Lens was changed");
            price();
        });
    });
    let checkbox = document.querySelectorAll("#checkbox input");
    checkbox.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});